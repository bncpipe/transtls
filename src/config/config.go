package config

import (
	"encoding/json"
	"errors"
	"net"
	"os"
)

type Config struct {
	TransPort   uint16 `json:"trans_port"`
	LnAddr      string `json:"ln_addr"`
	ProxyAddr   string `json:"proxy_addr"`
	TLSKeyPath  string `json:"tls_key_path"`
	TLSCertPath string `json:"tls_cert_path"`
}

func ReadCfg(CfgPath string) (Config, error) {
	var cfg Config
	f, e := os.Open(CfgPath)
	if e != nil {
		return cfg, e
	}
	defer f.Close()
	if e := json.NewDecoder(f).Decode(&cfg); e != nil {
		return cfg, e
	}
	if cfg.TransPort < 1 {
		return cfg, errors.New("invalid transparent proxy port provided (<1)")
	}
	if c := net.ParseIP(cfg.ProxyAddr); c == nil || c.To4() == nil {
		return cfg, errors.New("proxy address is invalid (IPv4 only)")
	}
	if c := net.ParseIP(cfg.LnAddr); c == nil || c.To4() == nil {
		return cfg, errors.New("listen address is invalid (IPv4 only)")
	}
	return cfg, nil
}
