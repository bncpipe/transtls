package main

import (
	"crypto/tls"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"
	"transtls/config"
)

const SO_ORIGINAL_DST = 80

func handleConn(cfg *config.Config, oFd int, conn net.Conn) {
	defer conn.Close()
	var (
		bindAddr syscall.SockaddrInet4
		connAddr syscall.SockaddrInet4
	)
	dstAddr, e := syscall.GetsockoptIPv6Mreq(oFd, syscall.IPPROTO_IP, SO_ORIGINAL_DST)
	if e != nil {
		syscall.Close(oFd)
		return
	}
	syscall.Close(oFd)
	dstPort := binary.BigEndian.Uint16(dstAddr.Multiaddr[2:4])
	srcIP := conn.RemoteAddr().(*net.TCPAddr).IP.To4()
	bindAddr.Port = 0
	copy(bindAddr.Addr[:], srcIP)
	rConnFd, e := syscall.Socket(syscall.AF_INET, syscall.SOCK_STREAM, 0)
	if e != nil {
		return
	}
	if syscall.SetsockoptInt(int(rConnFd), syscall.SOL_IP, syscall.IP_TRANSPARENT, 1) != nil {
		log.Panicf("failed to set IP_TRANSPARENT")
	}
	if syscall.SetsockoptInt(int(rConnFd), syscall.SOL_IP, syscall.IP_FREEBIND, 1) != nil {
		log.Panicf("failed to set IP_FREEBIND")
	}
	if syscall.SetsockoptInt(int(rConnFd), syscall.SOL_SOCKET, syscall.SO_REUSEADDR, 1) != nil {
		log.Panicf("failed to set SO_REUSEADDR")
	}
	connAddr.Port = int(dstPort)
	pAddr := net.ParseIP(cfg.ProxyAddr).To4()
	copy(connAddr.Addr[:], pAddr)
	rConn, e := net.FileConn(os.NewFile(uintptr(rConnFd), ""))
	if e != nil {
		return
	}
	syscall.Bind(rConnFd, &bindAddr)
	syscall.Connect(rConnFd, &connAddr)
	go io.Copy(rConn, conn)
	io.Copy(conn, rConn)
}

func getTLSCfg(cfg *config.Config) *tls.Config {
	lnCert, e := tls.LoadX509KeyPair(cfg.TLSCertPath, cfg.TLSKeyPath)
	if e != nil {
		return nil
	}
	return &tls.Config{
		Certificates: []tls.Certificate{lnCert},
	}
}

func reloadSignalHandler(sigChan chan os.Signal, cfg *config.Config, orig **tls.Config) {
	var c *tls.Config
	for {
		_ = <-sigChan
		c = getTLSCfg(cfg)
		if c == nil {
			log.Println("failed to reload TLS certs")
			continue
		}
		*orig = c
		log.Println("reloaded TLS certs")
	}
}

func main() {
	sigChan := make(chan os.Signal, 1)
	if len(os.Args) < 2 {
		fmt.Printf("usage: [%s] [cfg_path]\n", os.Args[0])
		os.Exit(1)
	}
	cfg, e := config.ReadCfg(os.Args[1])
	if e != nil {
		log.Panicf("failed to open config file: %s\n", e)
	}
	tlsCfg := getTLSCfg(&cfg)
	if tlsCfg == nil {
		log.Panicln("failed to load TLS certs")
	}
	lnAddr, e := net.ResolveTCPAddr("tcp", fmt.Sprintf("%s:%d", cfg.LnAddr, cfg.TransPort))
	tlsLn, e := net.ListenTCP("tcp", lnAddr)
	if e != nil {
		log.Panicf("failed to create ln: %s\n", e)
	}
	signal.Notify(sigChan, syscall.SIGHUP)
	go reloadSignalHandler(sigChan, &cfg, &tlsCfg)
	for {
		conn, e := tlsLn.Accept()
		if e != nil {
			continue
		}
		c, _ := conn.(*net.TCPConn).File()
		go handleConn(&cfg, int(c.Fd()), tls.Server(conn, tlsCfg))
	}
}
