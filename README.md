# transtls
A transparent reverse TCP TLS termination proxy that listens on a single port with a fixed destination address.

Required commands to use after modifying the configuration file:
```
ip rule add fwmark 10 lookup 100
ip route add local 0.0.0.0/0 dev lo table 100
# only needed if the dest IP addr is not on the same system, but you'll need something like "ip rule add from X.X.X.X/32 lookup 100" and net.ipv4.conf.all.route_localnet=1 otherwise
iptables -t mangle -A PREROUTING -p tcp -m socket -j MARK --set-xmark 0xa
```
Adjust the following kernel parameters:
```
net.ipv4.ip_forward=1
net.ipv4.ip_nonlocal_bind=1
net.ipv4.conf.default.rp_filter=0
net.ipv4.conf.default.accept_source_route=1
```
As for forwarding incoming traffic to the proxy, use a DNAT rule like the one below:
`iptables -t nat -A PREROUTING -d [incoming_ip]/32 -p tcp --dport [port][:end_port] -j DNAT --to-destination [proxy_ln_addr]:[proxy_port]`

## Why?
stunnel, socat (forking), and nginx streams, do not support this particular kind of operation. With nginx, it's worse, it has to spawn up a listener per port, if you use a range; this can lead to 100% CPU usage with merely 100 connections (tested with 2000-10000 port range)!

## Is it possible to reload the certificate via a process signal?
Yes, send a SIGHUP signal to the process: `kill -1 [pid]` or `pkill -1 [process_name]`.

