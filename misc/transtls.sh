#!/bin/sh
### BEGIN INIT INFO
# Provides:       transtls
# Required-Start: $network $remote_fs $syslog
# Required-Stop:  $network $remote_fs $syslog
# Default-Start:  2 3 4 5
# Default-Stop:   0 1 6
# Description:    Transparent TLS termination proxy
### END INIT INFO
set +e
PATH=/usr/bin:/bin:/sbin
DAEMON=/usr/bin/transtls
CFG_PATH="/etc/trantls.json"
NAME=transtls
DESC="Transparent TLS termination proxy"

. /lib/lsb/init-functions

start() {
    start-stop-daemon --start --quiet --pidfile /run/transtls/$NAME.pid --exec $DAEMON --test > /dev/null || return 1
    start-stop-daemon --start --quiet --pidfile /run/transtls/$NAME.pid --exec $DAEMON -- $CFG_PATH || return 2
}
stop() {
    start-stop-daemon --stop --quiet --pidfile /run/transtls/$NAME.pid --name $NAME
}
status() {
    start-stop-daemon --start --quiet --pidfile /run/transtls/$NAME.pid --exec $DAEMON --test > /dev/null
    case "$?" in
        0) [[ -e "/var/run/transtls/$NAME.pid" ]] && return 1; return 3;;
        1) return 0;;
        *)return 4;;
    esac
}
case "$1" in 
    start)
        log_daemon_msg "Starting $NAME"
        start
        case "$?" in
            0)
                log_end_msg 0
                exit 0
                ;;
            1)
                log_success_msg "(already running)"
                exit 0
                ;;
            *)
                log_end_msg 1
                exit 1
                ;;
        esac
        ;;
    stop)
        stop
        log_daemon_msg "Stopping $NAME"
        log_end_msg 0
        ;;
    restart)
        stop
        start
        log_daemon_msg "Restarting $NAME"
        ;;
    reload)
        kill -1 $(cat "/run/transtls/$NAME.pid")
        log_daemon_msg "Reloading $NAME"
        ;;
    *)
        echo "Usage: /etc/init.d/$NAME {start|stop|reload}" >&2
        exit 3
        ;;
esac

